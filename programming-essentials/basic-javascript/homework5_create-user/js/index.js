"use strict";

// with advance task option 1

let createNewUser = function() {
    let fName = prompt("Enter your first name: ");
    let lName = prompt("Enter your last name: ");
    let newUser = {
        _firstName: fName,
        _lastName: lName,
        set firstName(value) {
            this._firstName = value; 
        },
        get firstName() {
            return this._firstName;
        },
        set lastName(value) {
            this._lastName = value; 
        },
        get lastName() {
            return this._lastName;
        },
        getLogin () {
            return (this._firstName[0] + this._lastName).toLowerCase();
        }
    };
    return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.firstName);
console.log(user.lastName);
console.log(user.getLogin());
user.firstName = 'SERGEI'; //set
console.log(user.firstName); //get
user.lastName = 'POLISHCHUK'; //set
console.log(user.lastName); //get
console.log(user);
console.log(user.getLogin());

// with advance task option 2
/* 
function createNewUser() {
    let fName = prompt("Enter your first name: ");
    let lName = prompt("Enter your last name: ");
    return {
        _firstName: fName,
        _lastName: lName,
        set firstName(value) {
            this._firstName = value; 
        },
        get firstName() {
            return this._firstName;
        },
        set lastName(value) {
            this._lastName = value; 
        },
        get lastName() {
            return this._lastName;
        },
        getLogin () {
            return (this._firstName[0] + this._lastName).toLowerCase();
        }
    }
}
let user = createNewUser();
console.log(user);
console.log(user.firstName);
console.log(user.lastName);
console.log(user.getLogin());
user.firstName = 'SERGEI'; //set
console.log(user.firstName); //get
user.lastName = 'POLISHCHUK'; //set
console.log(user.lastName); //get
console.log(user);
console.log(user.getLogin());
 */


// with advance task option 3
/* 
let createNewUser = function () {
    let fName = prompt("Enter your first name: ");
    let lName = prompt("Enter your last name: ");
    let newUser = {
        _firstName: fName,
        _lastName: lName,
        set firstName(value) {
            this._firstName = value; 
        },
        get firstName() {
            return this._firstName;
        },
        set lastName(value) {
            this._lastName = value; 
        },
        get lastName() {
            return this._lastName;
        },
        getLogin () {
            return (this._firstName[0] + this._lastName).toLowerCase();
        }
    };
    return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.firstName);
console.log(user.lastName);
console.log(user.getLogin());

const property = prompt("What property do you want to change?");
const newValue = prompt("To what value?");
user[property] = newValue
console.log(user);
 */


// with advance task option 4
/* 
let createNewUser = function () {
    let fName = prompt("Enter your first name: ");
    let lName = prompt("Enter your last name: ");
    let newUser = {
        _firstName: fName,
        _lastName: lName,
        set firstName(value) {
            this._firstName = value; 
        },
        get firstName() {
            return this._firstName;
        },
        set lastName(value) {
            this._lastName = value; 
        },
        get lastName() {
            return this._lastName;
        },
        getLogin () {
            return (this._firstName[0] + this._lastName).toLowerCase();
        }
    };
    return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.firstName);
console.log(user.lastName);
console.log(user.getLogin());
let property = prompt("What property do you want to change?");
while (!(property in user)) {
  property = prompt(`${property} the object does not have. Please write the correct object property`);
  if(property === null) {
    break;
  }
}
if (property !== null) {
  const newValue = prompt("What value do you want to assign to this property?");
  user[property] = newValue;
}
console.log(user);
 */

// with advance task option 5 without set & get 
/* 
let createNewUser = function () {
    let fName = prompt("Enter your first name: ");
    let lName = prompt("Enter your last name: ");
    let newUser = {
        firstName: fName,
        lastName: lName,
        getLogin () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    };
    return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());

const user2 = {};    

user2.firstName = user.firstName;
user2.lastName = user.lastName;

user.firstName = "SERGEI";
console.log(user.firstName);
console.log(user2.firstName);

user.lastName = "POLISHCHUK";
console.log(user.lastName);
console.log(user2.lastName);

console.log(user);
console.log(user.getLogin());
 */


// without advance task option 1
/* 
function createNewUser() {
    let firstName = prompt("Enter your first name: ");
    let lastName = prompt("Enter your last name: ");
    return {
        firstName,
        lastName,
        getLogin () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    }
}
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
 */

//without advance task option 2
/* 
let createNewUser = function () {
    let firstName = prompt("Enter your first name: ");
    let lastName = prompt("Enter your last name: ");
    let newUser = {
        firstName,
        lastName,
        getLogin: function() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    };
    return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin()); 
 */



