/* 
- How can I declare a variable in Javascript?
 with var , let , or const

- What is the difference between the prompt 
function and the confirm function?

A prompt shows a message asking the user to input text,
It returns the text or, if Cancel button or Esc is clicked, null.
A confirm shows a message and waits for the user to press “OK” or “Cancel”,
It returns true for OK and false for Cancel/Esc.

- What is implicit type conversion? Give one example.

In certain situations, JavaScript automatically converts one data type to another (to the right type),
JavaScript is very flexible, it will convert the type of a value as needed automatically.

1: Implicit Conversion to String
numeric string used with + gives string type
When a number is added to a string, 
JavaScript converts the number to a string before concatenation.
2: Implicit Conversion to Number
numeric string used with - , / , * results number type
3: Non-numeric String Results to NaN
non-numeric string used with - , / , * results to NaN
4: Implicit Boolean Conversion to Number
if boolean is used, true is 1, false is 0
JavaScript considers 0 as false and all non-zero number as true. And, 
if true is converted to a number, the result is always 1.
5: null Conversion to Number
null is 0 when used with number
6: undefined used with number, boolean or null
Arithmetic operation of undefined with number, boolean or null gives NaN
*/

"use strict";

// (1)
let name = "mohamed";
let admin = name;
console.log(admin);


// (2)
let days = 3;
let seconds = (days * 24) * 60 * 60;
console.log(seconds);



// (3)
let userName = prompt("Please,type your name!", '');
console.log(userName);
 