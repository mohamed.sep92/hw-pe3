"use strict";

// 1 - Find all paragraphs on the page and set the background color to #ff0000
let elements1 = document.querySelectorAll('p');
elements1.forEach(item => {
    item.style.backgroundColor = '#ff0000';
});



// 2 - Find the element with id="optionsList". Output to the console. Find the parent element and output to the console. Find child nodes, if any, and output the node names and types to the console.

let elem2 = document.querySelector('#optionsList');
console.log(elem2);
console.log(elem2.parentElement);

let nodes = elem2.childNodes;
nodes.forEach(item => {
    console.log(`${item.nodeName} : ${item.nodeType}`);
});


// 3 - Set the following paragraph as the content of the element with the testParagraph id - This is a paragraph

let element3 = document.getElementById("testParagraph");
element3.textContent = "This is a paragraph"; // or innerText or innerHTML



// 5 - nested in an element li with the main-header class and display them in the console. Assign a new nav-item class to each element.

let elements5 = document.querySelectorAll('.main-header li');
console.log(elements5);
elements5.forEach(item => {
    item.classList.add('nav-item');
});





// 6 - Find all elements with-options-list-title class. Remove this class from these elements.

let elements6 = document.querySelectorAll('.options-list-title');
elements6.forEach(item => {
    item.classList.remove('options-list-title');
});









