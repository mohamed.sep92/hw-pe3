"use strict";


const ACTIVE_CLASS = 'btn-activated';
const button1 = document.querySelector('.btn-1');
const button2 = document.querySelector('.btn-2');
const button3 = document.querySelector('.btn-3');
const button4 = document.querySelector('.btn-4');
const button5 = document.querySelector('.btn-5');
const button6 = document.querySelector('.btn-6');
const button7 = document.querySelector('.btn-7');

document.addEventListener('keydown', handleKeyboard);


function handleKeyboard(e) {
    if (e.code == 'Enter') {
        button1.classList.add(ACTIVE_CLASS);
    } else if (e.code !== 'Enter') {
        button1.classList.remove(ACTIVE_CLASS);
    }
    if (e.code == 'KeyS') {
        button2.classList.add(ACTIVE_CLASS);
    } else if (e.code !== 'KeyS') {
        button2.classList.remove(ACTIVE_CLASS);
    }
    if (e.code == 'KeyE') {
        button3.classList.add(ACTIVE_CLASS);
    } else if (e.code !== 'KeyE') {
        button3.classList.remove(ACTIVE_CLASS);
    }
    if (e.code == 'KeyO') {
        button4.classList.add(ACTIVE_CLASS);
    } else if (e.code !== 'KeyO') {
        button4.classList.remove(ACTIVE_CLASS);
    }
    if (e.code == 'KeyN') {
        button5.classList.add(ACTIVE_CLASS);
    } else if (e.code !== 'KeyN') {
        button5.classList.remove(ACTIVE_CLASS);
    }
    if (e.code == 'KeyL') {
        button6.classList.add(ACTIVE_CLASS);
    } else if (e.code !== 'KeyL') {
        button6.classList.remove(ACTIVE_CLASS);
    }
    if (e.code == 'KeyZ') {
        button7.classList.add(ACTIVE_CLASS);
    } else if (e.code !== 'KeyZ') {
        button7.classList.remove(ACTIVE_CLASS);
    }
};














