"use strict";

function makeList(arr, option = document.body) {

    let list = document.createElement("ul");
    option.prepend(list);
    
    arr.forEach((item) => {
        let li = document.createElement("li");
        li.innerHTML = item;
        list.append(li);
    }
    )
};

makeList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);




