"use strict";

//option 1 without method map
let data = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let list = document.querySelector('ul');

data.forEach((item) => {
    let li = document.createElement("li");
    li.innerText = item;
    list.append(li);
});


//option 2 without method map 
// creeate a nother list tag
/* 
let data = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let h1 = document.querySelector('h1');  
let list = document.createElement("ul"); // or any tag such as div 
document.body.prepend(list);
list.before(h1);

data.forEach((item) => {
    let li = document.createElement("li");
    li.innerHTML = item;
    list.append(li);
});
 */


//option 3 without method map
/* 
function makeList() {
    let listData = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"],
    listContainer = document.createElement('div'),
    listElement = document.createElement('ul'),
    numberOfListItems = listData.length,
    listItem,
    i;
    document.getElementsByTagName('body')[0].prepend(listContainer);
    listContainer.append(listElement);
    for (i = 0; i < numberOfListItems; ++i) {
        listItem = document.createElement('li');
        listItem.innerHTML = listData[i];
        listElement.append(listItem);
    }
}
makeList();
  */


// with method map 1
/* 
let data = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let list = document.getElementById("myList");

list.innerHTML = data.map(function (data) {
	return '<li>' + data;
}).join('');
 */


// with method map 2
/* 
let data = ["1", "2", "3", "sea", "user", 23];

let myList = document.querySelector('#myList');

myList.innerHTML = '<ul>' + data.map(function (data) {
	return '<li>' + data + '</li>';
}).join('') + '</ul>'; 
 */














