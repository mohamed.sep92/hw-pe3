/*
1-why programming requires loops?

Loops are handy, if you want to run the same code over and 
over again, each time with a different value.

2-Describe in which situations you used this or that cycle in JS?

for - loops through a block of code a number of times
for/in - loops through the properties of an object
for/of - loops through the values of an iterable object
while - loops through a block of code while a specified condition is true
do/while - also loops through a block of code while a specified condition is true

3-What is explicit and implicit reduction (conversion) of data types in JS?

Implicit Conversion - automatic type conversion
Explicit Conversion - manual type conversion

JavaScript Implicit Conversion
In certain situations, JavaScript automatically converts one data 
type to another (to the right type). This is known as implicit conversion.
Implicit Conversion to String, Implicit Conversion to Number,
Non-numeric String Results to NaN, Implicit Boolean Conversion to Number,
null Conversion to Number, undefined used with number, boolean or null.

explicit type conversions are done using built-in methods,
Convert to Number Explicitly, Convert to String Explicitly,
Convert to Boolean Explicitly.
*/

"use strict";

//question loop untill user enter a number 

let getNumber = prompt("Please, enter a number!");
while (getNumber === "" || isNaN(getNumber)) {
    console.log("Sorry, no numbers");
    getNumber = prompt("Please, enter a number!");
}
for (let i = 0; i <= getNumber;) {
    console.log(i);
    i += 5;
}

/* 
// option 2
let getNumber = prompt("Please, enter a number!");
while (getNumber === "" || isNaN(getNumber)) {
    console.log("Sorry, no numbers");
    getNumber = prompt("Please, enter a number!");
}
for (let i = 0; i <= getNumber; i += 5) {
    if (true) {
        console.log(i);
    }
} */




