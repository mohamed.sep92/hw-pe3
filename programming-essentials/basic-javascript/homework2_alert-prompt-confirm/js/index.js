/* 1-What are the data types in Javascript?

Primitive data type: String, Number, Boolean, Undefined, Null
Non-primitive (reference) data type: Object, Array, RegExp

2-What is the difference between == and ===?

The ‘==’ operator tests for abstract equality,
it does the necessary type conversions before doing the equality comparison,
in JavaScript is used for comparing two variables, 
but it ignores the datatype of variable.

But the ‘===’ operator tests for strict equality,
it will not do the type conversion hence 
if the two values are not of the same type, 
when compared, it will return false,
is used for comparing two variables, 
but this operator also checks datatype and compares two values.

3-What is an operator?

An operator performs some operation on single or
multiple operands (data value) and produces a result:
Arithmetic Operators, Comparison Operators, Logical Operators, 
Assignment Operators, Conditional Operators, Ternary Operator.
*/



"use strict";
// i have an issue with the ( condition ) in while, could you please help me with it


let userName;
userName = prompt("Please,type your name!");
while (userName === "" || !isNaN(userName)) {
    console.log("Sorry, no name");
    userName = prompt("Please,type your name!");
}
console.log(`user name: ${userName}`);

let userAge;
userAge = prompt("Please, type your age!");
while (userAge === "" || isNaN(userAge)) {
    console.log("Sorry, no number");
    userAge = prompt("Please, type your age!");
}
console.log(`user age: ${userAge}`);

let userConfirm;

if (userAge < 18) {
    console.log("age less 18");
    alert("You are not allowed to visit this website");
} else if (userAge > 22) {
    console.log("age more 22");
    alert(`Welcome, ${userName}`);
} else if (userAge <= 22 && userAge >=18) {
    console.log("age from 18 to 22");
    userConfirm = confirm("Are you sure you want to continue?");
    if (userConfirm === true) {
        console.log("user pressed OK!");
        alert(`Welcome, ${userName}`);
    } else {
        console.log("user pressed cancel!");
        alert("You are not allowed to visit this website");
    }
}


/* 
//option 2
let userName;
let userAge;
do {
    userName = prompt("Please,type your name!");
    userAge = prompt("Please, type your age!");
} while (userName === "" || Number(userName) || userAge === "" || isNaN(userAge));
let userConfirm;
if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge > 22) {
    alert(`Welcome, ${userName}`);
} else if (userAge <= 22 && userAge >=18) {
    userConfirm = confirm("Are you sure you want to continue?");
    if (userConfirm === true) {
        alert(`Welcome, ${userName}`);
    } else {
        alert("You are not allowed to visit this website");
    }
}
 */

/* 
// option 3
let userName;
let userAge;
do {
    userName = prompt("Please,type your name!");
    console.log(`user name: ${userName}`);
} while (Number(userName) || userName === "");

do {
    userAge = prompt("Please, type your age!");
    console.log(`user age: ${userAge}`);
} while (isNaN(userAge) || userAge === "");

let userConfirm;
if (userAge < 18) {
    console.log("age less 18");
    alert("You are not allowed to visit this website");
} else if (userAge > 22) {
    console.log("age more 22");
    alert(`Welcome, ${userName}`);
} else if (userAge <= 22 && userAge >=18) {
    console.log("age from 18 to 22");
    userConfirm = confirm("Are you sure you want to continue?");
    if (userConfirm === true) {
        console.log("user pressed OK!");
        alert(`Welcome, ${userName}`);
    } else {
        console.log("user pressed cancel!");
        alert("You are not allowed to visit this website");
    }
}
 */




