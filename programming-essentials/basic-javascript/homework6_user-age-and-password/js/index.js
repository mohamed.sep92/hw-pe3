"use strict";


let createNewUser = function () {
  let fName = prompt("Enter your first name: ");
  let lName = prompt("Enter your last name: ");
  let birthDay = prompt("Please enter your date of birth: ", "dd.mm.yyyy");
  let newUser = {
    firstName: fName,
    lastName: lName,
    birthDay,
    getLogin(){
      return ((this.firstName[0] + this.lastName).toLowerCase());
    },
    getAge() {
      let natalDay = this.birthDay.slice(6);
      let date = new Date(natalDay);
      let ageInMilliseconds = Date.now() - date;
      let years = Math.floor((ageInMilliseconds) / (1000 * 60 * 60 * 24 * 365));
      return ("Hello, " + "you are " + years + " years old!");
    },
    getPassword() {
      return ((this.firstName[0].toUpperCase()) + (this.lastName.toLowerCase()) + (this.birthDay.slice(6)));
    }
  };
  return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());






///////////////////////////
// display  method function date in the format dd.mm.yy
/* 
let createNewUser = function () {
  let fName = prompt("Enter your first name: ");
  let lName = prompt("Enter your last name: ");
  let birthDay = prompt("Please enter your date of birth: ", "dd.mm.yyyy");
  let newUser = {
    firstName: fName,
    lastName: lName,
    birthDay,
    getLogin(){
      return ((this.firstName[0] + this.lastName).toLowerCase());
    },
    formateDate(date) {
      let dd = date.getDate();
      if (dd < 10) dd = '0' + dd;
      let mm = date.getMonth() + 1;
      if (mm < 10) mm = '0' + mm;
      let yy = date.getFullYear();
      if (yy < 10) yy = '0' + yy;
      return dd + '.' + mm + '.' + yy;
    },
    getPassword() {
      return ((this.firstName[0].toUpperCase()) + (this.lastName.toLowerCase()) + (this.birthDay.slice(6)));
    }
  };
  return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
let d = new Date(1992, 8, 10);
console.log(user.formateDate(d));
console.log(user.getPassword()); 
*/

////////////////////////////////////

/* 
function addLeadingZero(d) {
  return (d < 10) ? 'o' + d : d;
}

const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

function getUserTime(t = new Date()) {
  let Y = t.getFullYear();
  let M = addLeadingZero(t.getMonth() + 1);
  // if (M < 10) M = '0' + M;
  let D = addLeadingZero(t.getDate());
  let d = days[t.getDay()];
  let h = addLeadingZero(t.getHours());
  let m = addLeadingZero(t.getMinutes());

  console.log(Y, M, D, d, h, m);
  return `${Y} . ${M} . ${D} . ${h} . ${m} . (${d})`;
}
console.log(getUserTime(new Date()));
 */


