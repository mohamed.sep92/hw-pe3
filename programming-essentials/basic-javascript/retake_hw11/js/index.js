"use strict";

const form = document.querySelector("#form");
const username = document.querySelector("#username");
const email = document.querySelector("#email");
const password = document.querySelector("#password");
const password2 = document.querySelector("#password2");

const icon = document.querySelector(".show");
icon.addEventListener("click", () => {
    if ((password.type === "password") && (password2.type === "password")) {
        password.type = "text";
        password2.type = "text";
        icon.classList.replace("fa-eye-slash", "fa-eye");
    } else {
        password.type = "password";
        password2.type = "password";
        icon.classList.replace("fa-eye", "fa-eye-slash");
    }
});

form.addEventListener("submit", (e) => {
    e.preventDefault();
    checkInputs();
});

function checkInputs() {
    // trim to remove the whitespaces
    const usernameValue = username.value.trim();
    const emailValue = email.value.trim();
    const passwordValue = password.value.trim();
    const password2Value = password2.value.trim();

    if (usernameValue === "") { // or we can instead add attribute required in html 
        setErrorFor(username, "Username cannot be blank"); //show error & add error class
    } else {
        
        setSuccessFor(username); // add success class
    }

    if (emailValue === "") { // or we can instead add attribute required in html 
        setErrorFor(email, "Email cannot be blank");
    } else if (!isEmail(emailValue)) {
        setErrorFor(email, "Not a valid email");
    } else {
        setSuccessFor(email);
    }

    if (passwordValue === "") { // or we can instead add attribute required in html 
        setErrorFor(password, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else if (passwordValue.length < 8) {
        setErrorFor(password, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else if (!(/\d/.test(passwordValue) && /[a-z]/.test(passwordValue) && /[A-Z]/.test(passwordValue) && /[-+_!@#$%^&*.,?]/.test(passwordValue))) {
        setErrorFor(password, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else {
        setSuccessFor(password);
    }

    if (password2Value === "") { // or we can instead add attribute required in html 
        setErrorFor(password2, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else if (passwordValue !== password2Value) {
        setErrorFor(password2, "please enter the same password");
    } else if (password2Value.length < 8) {
        setErrorFor(password2, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else if (!(/\d/.test(password2Value) && /[a-z]/.test(password2Value) && /[A-Z]/.test(password2Value) && /[-+_!@#$%^&*.,?]/.test(password2Value))) {
        setErrorFor(password2, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else {
        setSuccessFor(password2);
    }

    if (
        passwordValue === password2Value &&
        usernameValue !== "" &&
        emailValue !== "" &&
        isEmail(emailValue) &&
        passwordValue !== "" &&
        passwordValue.length > 8 &&
        /\d/.test(passwordValue) &&
        /[a-z]/.test(passwordValue) &&
        /[A-Z]/.test(passwordValue) &&
        /[-+_!@#$%^&*.,?]/.test(passwordValue) &&
        password2Value !== "" &&
        password2Value.length > 8 &&
        /\d/.test(password2Value) &&
        /[a-z]/.test(password2Value) &&
        /[A-Z]/.test(password2Value) &&
        /[-+_!@#$%^&*.,?]/.test(password2Value)
    ) {
        alert("you are welcome");
    }
}

function setErrorFor(input, message) {
    const formControl = input.parentElement; // .form-control
    const small = formControl.querySelector("small");
    formControl.className = "form-control error"; // add error class
    small.innerText = message; // add error message inside small
}

function setSuccessFor(input) {
    const formControl = input.parentElement; // .form-control
    formControl.className = "form-control success"; // add success class
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

