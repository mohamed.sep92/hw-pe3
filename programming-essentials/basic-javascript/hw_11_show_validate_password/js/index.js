"use strict";

const form = document.querySelector("#form");
const username = document.querySelector("#username");
const email = document.querySelector("#email");
const password = document.querySelector("#password");
const password2 = document.querySelector("#password2");

function Toggle() {
    if (password.type === "password") {
        password.type = "text";
        document.querySelector(".icon-password-slash1").style.visibility = "hidden";
        document.querySelector(".icon-password1").style.visibility = "visible";
    } else {
        password.type = "password";
        document.querySelector(".icon-password-slash1").style.visibility = "visible";
        document.querySelector(".icon-password1").style.visibility = "hidden";
    }
}

function Toggle2() {
    if (password2.type === "password") {
        password2.type = "text";
        document.querySelector(".icon-password-slash2").style.visibility = "hidden";
        document.querySelector(".icon-password2").style.visibility = "visible";
    } else {
        password2.type = "password";
        document.querySelector(".icon-password-slash2").style.visibility = "visible";
        document.querySelector(".icon-password2").style.visibility = "hidden";
    }
}

form.addEventListener("submit", (e) => {
    e.preventDefault();
    checkInputs();
});

function checkInputs() {
    // trim to remove the whitespaces
    const usernameValue = username.value.trim();
    const emailValue = email.value.trim();
    const passwordValue = password.value.trim();
    const password2Value = password2.value.trim();

    if (usernameValue === "") {
        setErrorFor(username, "Username cannot be blank"); //show error & add error class
    } else {
        
        setSuccessFor(username); // add success class
    }

    if (emailValue === "") {
        setErrorFor(email, "Email cannot be blank");
    } else if (!isEmail(emailValue)) {
        setErrorFor(email, "Not a valid email");
    } else {
        setSuccessFor(email);
    }

    if (passwordValue === "") {
        setErrorFor(password, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else if (passwordValue.length < 8) {
        setErrorFor(password, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else if (!(/\d/.test(passwordValue) && /[a-z]/.test(passwordValue) && /[A-Z]/.test(passwordValue) && /[-+_!@#$%^&*.,?]/.test(passwordValue))) {
        setErrorFor(password, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else {
        setSuccessFor(password);
    }

    if (password2Value === "") {
        setErrorFor(password2, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else if (passwordValue !== password2Value) {
        setErrorFor(password2, "please enter the same password");
    } else if (password2Value.length < 8) {
        setErrorFor(password2, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else if (!(/\d/.test(password2Value) && /[a-z]/.test(password2Value) && /[A-Z]/.test(password2Value) && /[-+_!@#$%^&*.,?]/.test(password2Value))) {
        setErrorFor(password2, "Please enter at least 8 char with number, symbol, capital and small letter.");
    } else {
        setSuccessFor(password2);
    }
    // show success message: alert('you are welcome');
}

function setErrorFor(input, message) {
    const formControl = input.parentElement; // .form-control
    const small = formControl.querySelector("small");
    formControl.className = "form-control error"; // add error class
    small.innerText = message; // add error message inside small
}

function setSuccessFor(input) {
    const formControl = input.parentElement; // .form-control
    formControl.className = "form-control success"; // add success class
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}
