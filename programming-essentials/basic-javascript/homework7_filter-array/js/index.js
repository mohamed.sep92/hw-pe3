"use strict";

let arr = ['hello', 'world', 23, '23', null];
let filterBy = arr.filter(item => typeof item !== 'string');
console.log( filterBy );


/* 
// another way 
let arr = ['hello', 'world', 23, '23', null];
let filterBy = arr.filter(function (item) {
    return typeof item !== 'string';
});
console.log(filterBy);
 */


