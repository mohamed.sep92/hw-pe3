"use strict";

//option 1 with the advance task
let number1;
let number2;
let operator;
function calc(number1, number2, operator) {
    do {
        number1 = prompt("Enter first number:");
        // console.log(`the first number: ${number1}`);
        number2 = prompt("Enter second number:");
        // console.log(`the second number: ${number2}`);
    } while (number1 === "" || isNaN(number1) || number2 === "" || isNaN(number2));
        operator = prompt("Enter operator to perform the calculation ( either +, -, * or / ):");
        // console.log(`the operator: ${operator}`);
    switch (operator) {
        case '+':
            return `Result is: ${Number(number1) + Number(number2)}`;
        
        case '-':
            return `Result is: ${number1 - number2}`;
        
        case '*':
            return `Result is: ${number1 * number2}`;
            
        case '/':
            return `Result is: ${number1 / number2}`;
        
        default:
            return ('error');
    }
}
console.log(calc(number1, number2, operator));


// option 2 with another method, no check for correctness 
/* 
let number1;
let number2;
let operator;
function calc(number1, number2, operator) {
    number1 = +prompt('Enter the first number:');
    // console.log(`the first number: ${number1}`);
    number2 = +prompt('Enter the second number:');
    // console.log(`the second number: ${number2}`);
    operator = prompt('Enter operator to perform the calculation ( either +, -, * or / ):');
    // console.log(`the operator: ${operator}`);
    if (operator == '+') {
        console.log(`Result is: ${number1 + number2}`);
    }
    else if (operator == '-') {
        console.log(`Result is: ${number1 - number2}`);  
    }
    else if (operator == '*') {
        console.log(`Result is: ${number1 * number2}`);   
    }
    else if (operator == '/') {  
        console.log(`Result is: ${number1 / number2}`);  
    } else {
        console.log("error");
    } 
}
calc(number1, number2, operator);  */