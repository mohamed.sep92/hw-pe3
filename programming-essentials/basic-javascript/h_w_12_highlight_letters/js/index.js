"use strict";


const ACTIVE_CLASS = 'btn-activated';

const container = document.querySelector('.btn-wrapper');
const buttons = {
    one: container.querySelector('.btn-1'),
    two: container.querySelector('.btn-2'),
    three: container.querySelector('.btn-3'),
    four: container.querySelector('.btn-4'),
    five: container.querySelector('.btn-5'),
    six: container.querySelector('.btn-6'),
    seven: container.querySelector('.btn-7')
}

const keyButtonMap = {
    'Enter': buttons.one,
    'KeyS': buttons.two,
    'KeyE': buttons.three,
    'KeyO': buttons.four,
    'KeyN': buttons.five,
    'KeyL': buttons.six,
    'KeyZ': buttons.seven
};

document.addEventListener('keydown', handleKeyboard);


function handleKeyboard(e) {
    if (e.defaultPrevented) return
    if (keyButtonMap.hasOwnProperty(e.code)) {
        pressButton(keyButtonMap[e.code])
    } else {
        return
    }
    e.preventDefault();
};

function pressButton(button) {
    if (button.classList.contains(ACTIVE_CLASS)) {
        button.classList.remove(ACTIVE_CLASS); // console.log('reset');
        return;
    }
    button.classList.add(ACTIVE_CLASS); // console.log('add');
    
    
};












