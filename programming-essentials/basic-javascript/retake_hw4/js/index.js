"use strict";

let number1;
let number2;
let operator;
do {
    number1 = prompt("Enter first number:");
} while (number1 === "" || number1 === null || isNaN(number1));

do {
    number2 = prompt("Enter second number:");
} while (number2 === "" || number2 === null || isNaN(number2));

operator = prompt("Enter operator to perform the calculation ( either +, -, * or / ):");
function calc(number1, number2, operator) {
    switch (operator) {
        case '+':
            return Number(number1) + Number(number2);

        case '-':
            return number1 - number2;

        case '*':
            return number1 * number2;

        case '/':
            return number1 / number2;

        default:
            return ('error');
    }
}
console.log(calc(number1, number2, operator));



/* 
let number1;
let number2;
let operator;
do {
    number1 = prompt("Enter first number:");
    number2 = prompt("Enter second number:");
} while (number1 === "" || isNaN(number1) || number2 === "" || isNaN(number2));
operator = prompt("Enter operator to perform the calculation ( either +, -, * or / ):");
function calc(number1, number2, operator) {
    if (operator == "+") {
        return `Result is: ${Number(number1) + Number(number2)}`;
    } else if (operator == "-") {
        return `Result is: ${number1 - number2}`;
    } else if (operator == "*") {
        return `Result is: ${number1 * number2}`;
    } else if (operator == "/") {
        return `Result is: ${number1 / number2}`;
    } else {
        return 'error';
    }
}
console.log(calc(number1, number2, operator)); */


