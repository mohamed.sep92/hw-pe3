"use strict";

let createNewUser = function () {
  let fName = prompt("Enter your first name: ");
  let lName = prompt("Enter your last name: ");
  let birthDay = prompt("Please enter your date of birth: ", "dd.mm.yyyy");
  let newUser = {
    firstName: fName,
    lastName: lName,
    birthDay,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge() {
      let userInput = this.birthDay;
      let dob = new Date(userInput);

      if (userInput == null || userInput == "") {
        return false;
      }
      //execute if user entered a date
      else {
        //extract and collect only date from date-time string
        let mdate = userInput.toString();
        let dobYear = parseInt(mdate.slice(6, 10));
        let dobMonth = parseInt(mdate.slice(3, 5));
        let dobDate = parseInt(mdate.slice(0, 2));

        //get the current date from system
        let today = new Date();
        //date string after broking
        let natalDay = new Date(dobYear, dobMonth - 1, dobDate);

        //calculate the difference of dates
        let diffInMillisecond = today.valueOf() - natalDay.valueOf();

        //convert the difference in milliseconds and store in day and year variable
        let year_age = Math.floor(diffInMillisecond / 31536000000);
        let day_age = Math.floor((diffInMillisecond % 31536000000) / 86400000);

        let month_age = Math.floor(day_age / 30);

        // day_age = day_age % 30;

        //DOB is greater than today date, generate an error: Invalid date
        if (dob > today) {
          return "Invalid date input - Please try again!";
        } else {
          return (
            year_age + " years " + month_age + " months "
            // year_age + " years " + month_age + " months " + day_age + " days"
          );
        }
      }
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthDay.slice(6)
      );
    },
  };
  return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

///////////////////////////
/* let createNewUser = function () {
  let fName = prompt("Enter your first name: ");
  let lName = prompt("Enter your last name: ");
  let birthDay = prompt("Please enter your date of birth: ", "dd.mm.yyyy");
  let newUser = {
    firstName: fName,
    lastName: lName,
    birthDay,
    getLogin(){
      return ((this.firstName[0] + this.lastName).toLowerCase());
    },
    getAge() {
      let natalDay = this.birthDay.slice(6);
      let date = new Date(natalDay);
      let ageInMilliseconds = Date.now() - date;
      let years = Math.floor((ageInMilliseconds) / (1000 * 60 * 60 * 24 * 365));
      return ("Hello, " + "you are " + years + " years old!");
    },
    getPassword() {
      return ((this.firstName[0].toUpperCase()) + (this.lastName.toLowerCase()) + (this.birthDay.slice(6)));
    }
  };
  return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
 */

///////////////////////////
// display  method function date in the format dd.mm.yy
/* 
let createNewUser = function () {
  let fName = prompt("Enter your first name: ");
  let lName = prompt("Enter your last name: ");
  let birthDay = prompt("Please enter your date of birth: ", "dd.mm.yyyy");
  let newUser = {
    firstName: fName,
    lastName: lName,
    birthDay,
    getLogin(){
      return ((this.firstName[0] + this.lastName).toLowerCase());
    },
    formateDate(date) {
      let dd = date.getDate();
      if (dd < 10) dd = '0' + dd;
      let mm = date.getMonth() + 1;
      if (mm < 10) mm = '0' + mm;
      let yy = date.getFullYear();
      if (yy < 10) yy = '0' + yy;
      return dd + '.' + mm + '.' + yy;
    },
    getPassword() {
      return ((this.firstName[0].toUpperCase()) + (this.lastName.toLowerCase()) + (this.birthDay.slice(6)));
    }
  };
  return newUser;
};
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
let d = new Date(1992, 8, 10);
console.log(user.formateDate(d));
console.log(user.getPassword()); 
*/

////////////////////////////////////

/* 
function addLeadingZero(d) {
  return (d < 10) ? 'o' + d : d;
}

const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

function getUserTime(t = new Date()) {
  let Y = t.getFullYear();
  let M = addLeadingZero(t.getMonth() + 1);
  // if (M < 10) M = '0' + M;
  let D = addLeadingZero(t.getDate());
  let d = days[t.getDay()];
  let h = addLeadingZero(t.getHours());
  let m = addLeadingZero(t.getMinutes());

  console.log(Y, M, D, d, h, m);
  return `${Y} . ${M} . ${D} . ${h} . ${m} . (${d})`;
}
console.log(getUserTime(new Date()));
 */


