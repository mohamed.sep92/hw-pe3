"use strict";

function makeList(arr, option) {
    let h1 = document.querySelector('h1');
    let list = document.createElement("ul");
    option.prepend(list);
    list.before(h1);
    arr.forEach((item) => {
        let li = document.createElement("li");
        li.innerHTML = item;
        list.append(li);
    }
)};
makeList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], document.body);



