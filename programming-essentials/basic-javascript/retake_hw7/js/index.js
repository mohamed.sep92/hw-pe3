"use strict";

function filterBy(arr, dataType) {
    return arr.filter(item => {
        if (item === null) {
            return "null" !== dataType;
        } else {
            return typeof item !== dataType;
        }
    }
)};
console.log(filterBy(['hello', 'world', 23, '23', null], "null"));
console.log(filterBy(['hello', 'world', 23, '23', null], "object"));
console.log(filterBy(['hello', 'world', 23, '23', null], "string"));
console.log(filterBy(['hello', 'world', 23, '23', null], "number"));
console.log(filterBy(['hello', 'world', 23, '23', null], "undefined"));













