const url = "https://ajax.test-danit.com/api/swapi/films";
const listFilms = document.querySelector(".content");
const charactersName = "characters-";

function getResponse(url) {
  return fetch(url)
    .then((response) => response.json())
    .catch((error) => console.log(error));
}

function createFilmItems(films) {
  films.forEach((item) => {
    layoutFilm(
      item.id,
      item.name,
      item.episodeId,
      item.openingCrawl,
      item.characters
    );
  });
}

function getCharacters(charactersNameId, characters) {
  characters.forEach((url) => {
    getResponse(url)
      .then((item) => {
        document.querySelector(
          `.${charactersNameId}`
        ).innerHTML += `<li>${item.name}</li>`;
      })
      .catch((error) => console.log(error));
  });
}

function layoutFilm(id, name, episodeId, openingCrawl, characters) {
  let layout = `
  <div class="item__film">
        <h2 class="film__name">${name}</h2>
        <div class="film__characters">
          <span class="characters-title">Characters:</span>
          <ul class="characters-${id}"></ul>
        </div>
        <div class="film__episodeId">
          <span class="episodeId-title">Episode:</span>
          <span class="episodeId">${episodeId}</span>
        </div>
        <div class="film__openingCrawl">
          <span class="openingCrawl-title">Description:</span>
          <span class="openingCrawl">${openingCrawl}</span>
        </div>
      </div>
  `;
  listFilms.innerHTML += layout;
  getCharacters(charactersName + id, characters);
}

getResponse(url)
  .then((films) => createFilmItems(films))
  .catch((error) => console.log(error));
