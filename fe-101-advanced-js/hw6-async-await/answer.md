## Theoretical question

1. Explain how you understand asynchrony in Javascript ?

The asynchrony of code execution in Javascript is organized using the command execution stack, the command call stack, the web api - which is responsible for executing micro and macro tasks, and the event loop - a loop that moves tasks from the web api when they are triggered / finished / executed in call stack, from the call stack to the execution stack when the execution stack is freed. Synchronous code enters the execution stack first, then microtasks (event, promise ...), then macrotasks (setInterval, setTimeout ...)
