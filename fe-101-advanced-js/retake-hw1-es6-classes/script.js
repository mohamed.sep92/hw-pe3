class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }

  set name(name) {
    this._name = name;
  }
  set age(age) {
    this._age = age;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  set salary(salary) {
    return (this._salary = salary);
  }
  get salary() {
    return this._salary * 3;
  }

  set lang(lang) {
    return (this._lang = lang);
  }

  get lang() {
    return this._lang;
  }
}

const employee = new Employee('mo', 29, 400);
console.log(employee);
console.log(employee.age);
employee.age = 31;
console.log(employee.age);
console.log(employee.name);
console.log(employee.salary);

const programmer = new Programmer('yoda', 40, 1200, 'java');
console.log(programmer);
console.log(programmer.name);
console.log(programmer.age);
console.log(programmer.lang);
console.log(programmer.salary);

const programmer2 = new Programmer('grogu', 30, 700, 'js');
console.log(programmer2);
console.log(programmer2.name);
console.log(programmer2.age);
console.log(programmer2.lang);
console.log(programmer2.salary);







