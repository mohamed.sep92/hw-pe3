## Theoretical questions

Explain in your own words how you understand how prototype imitation works in Javascript?<br>

1. All JavaScript objects inherit properties and methods from a prototype, Prototypes are the mechanism by which JavaScript objects inherit features from one another, there are two prototypes in JavaScript: a) prototype: This is a special object which is assigned as property of any function we make in JavaScript, b)[[Prototype]]: This is a somehow-hidden property on every object which is accessed by the running context if some property which is being read on the object is not available, This property is a reference to the prototype of the function from which the object was made.

Why do you need to call super() in the constructor of the child class?<br>

2. In a child class, use super() to call its parent’s constructor and super.<methodName> to access its parent’s methods, The super keyword is used to call the constructor of its parent class to access the parent's properties and methods, is used to access properties on an object literal or class's Prototype, or invoke a superclass's constructor.


