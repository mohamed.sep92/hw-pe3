const root = document.querySelector('#root');
const validationValue = ['author', 'name', 'price'];
const books = [
  {
    author: 'Скотт Бэккер',
    name: 'Тьма, что приходит прежде',
    price: 70,
  },
  {
    author: 'Скотт Бэккер',
    name: 'Воин-пророк',
  },
  {
    name: 'Тысячекратная мысль',
    price: 70,
  },
  {
    author: 'Скотт Бэккер',
    name: 'Нечестивый Консульт',
    price: 70,
  },
  {
    author: 'Дарья Донцова',
    name: 'Детектив на диете',
    price: 40,
  },
  {
    author: 'Дарья Донцова',
    name: 'Дед Снегур и Морозочка',
  },
];

function validation(item) {
  for (const value of validationValue) {
    try {
      if (!item[value]) {
        throw new Error(`${value} not found`);
      }
    } catch (error) {
      console.log(error.message);
      return false;
    }
  }
  return true;
}

function setBooks(items) {
  const booksContainer = document.createElement('ul');
  booksContainer.classList.add('books');
  root.append(booksContainer);

  items.forEach((item) => {
    const itemContainer = document.createElement('li');
    itemContainer.classList.add('books__item');

    for (const value of validationValue) {
      const bookValue = document.createElement('div');
      bookValue.classList.add(value);
      bookValue.innerText = `${value} : ${item[value]}`;

      itemContainer.append(bookValue);
      booksContainer.append(itemContainer);
    }
  });
}

function getBooks(books) {
  const result = [];
  books.forEach((item) => {
    if (validation(item)) {
      result.push(item);
    }
  });
  setBooks(result);
}

getBooks(books);
