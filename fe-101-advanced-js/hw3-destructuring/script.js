/*
1. You have 2 arrays of strings, each with last names
clients. Create one array based on them, which
will be the union of two arrays without
duplicate customer names.
*/

const clients1 = [
  'Гилберт',
  'Сальваторе',
  'Пирс',
  'Соммерс',
  'Форбс',
  'Донован',
  'Беннет',
];
const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон'];

const allClients = [...new Set([...clients1, ...clients2])];
console.log('*** 1 ***');
console.log(allClients);

/*
2. Here is an array of `characters`, consisting of objects.
Each object describes one character.
Create an array based on it `charactersShortInfo`,
consisting of objects that only have
3 fields name, lastName and age.
*/

const characters = [
  {
    name: 'Елена',
    lastName: 'Гилберт',
    age: 17,
    gender: 'woman',
    status: 'human',
  },
  {
    name: 'Кэролайн',
    lastName: 'Форбс',
    age: 17,
    gender: 'woman',
    status: 'human',
  },
  {
    name: 'Аларик',
    lastName: 'Зальцман',
    age: 31,
    gender: 'man',
    status: 'human',
  },
  {
    name: 'Дэймон',
    lastName: 'Сальваторе',
    age: 156,
    gender: 'man',
    status: 'vampire',
  },
  {
    name: 'Ребекка',
    lastName: 'Майклсон',
    age: 1089,
    gender: 'woman',
    status: 'vempire',
  },
  {
    name: 'Клаус',
    lastName: 'Майклсон',
    age: 1093,
    gender: 'man',
    status: 'vampire',
  },
];

function getShortInfo(arr) {
  const result = [];
  arr.forEach(({ name, lastName, age }) => {
    result.push({ name, lastName, age });
  });
  return result;
}

const charactersShortInfo = getShortInfo(characters);
console.log('*** 2 ***');
console.log(charactersShortInfo);

/*
3. Write a destructuring assignment that:
 the name property will be assigned to the name variable
 the years property will be assigned to the age variable
 the isAdmin property will be assigned to a variable
isAdmin (false if there is no such property)
*/

const user1 = {
  name: 'John',
  years: 30,
};

const { name, years: age, isAdmin = false } = user1;

console.log('*** 3 ***');
console.log(name);
console.log(age);
console.log(isAdmin);

/*
4. Write code that will complete
dossier on the possible identity of Satoshi Nakamoto.
Modify `satoshi2018` objects,
`satoshi2019`, `satoshi2020` is not allowed.
*/

const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome',
};

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto',
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05',
};

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log('*** 4 ***');
console.log(fullProfile);

/*
5. Given an array of books. You need to add to it
another book without changing the existing array (in
as a result of the operation, a new array should be created).
*/

const books = [
  {
    name: 'Harry Potter',
    author: 'J.K. Rowling',
  },
  {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien',
  },
  {
    name: 'The witcher',
    author: 'Andrzej Sapkowski',
  },
];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin',
};

const newBooks = [...books, { ...bookToAdd }];
console.log('*** 5 ***');
console.log(newBooks);

/*
6. Given an `employee` object.Add the age properties to it and
salary without changing the original object (should be
created a new object that will include everything
required properties). Output the newly created
object to the console.
*/

const employee = {
  name: 'Vitalii',
  surname: 'Klichko',
};

const newEmployee = { ...employee, age: 51, salary: 5500 };
console.log('*** 6 ***');
console.log(newEmployee);

// 7. Modify the code to make it work

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

console.log('*** 7 ***');
console.log(value); // should print 'value'
console.log(showValue()); // should output 'showValue'

