## Theoretical question

Give a couple of examples when it is appropriate to use `try ... Catch` in the code.

1. The try statement defines a code block to run/try, The try statement allows to define a block of code to be tested for errors while it is being executed. The catch statement defines a code block to handle any error, The catch statement allows to define a block of code to be executed, if an error occurs in the try block. The JavaScript statements try and catch come in pairs. The throw statement defines a custom error, allows to create a custom error, throw an exception/error, The exception can be a JavaScript String, a Number, a Boolean or an Object, If we use throw together with try and catch, we can control program flow and generate custom error messages. The finally statement defines a code block to run regardless of the result, The finally statement lets us execute code, after try and catch, regardless of the result. for example: receiving data from the server, uploading files by the user, etc...

    try {
    Block of code to try
    }
    catch(err) {
    Block of code to handle errors
    }
    finally {
    Block of code to be executed regardless of the try / catch result
    }





