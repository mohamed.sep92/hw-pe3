## Assignment

Create an online store page.

For this, it is necessary to supplement the project created in the previous homework [homework1](../homework1/readme.md).

#### Technical requirements:
- Create an array with a collection of online store products.
- One product must contain the following data
  - Name
  - The price
  - Path to the picture (url on the Internet or path to the file in the `public` folder)
  - Article number (any numbers)
  - Color
- There must be at least 10 products in total. The theme of the store is any.
- Put the array in a JSON file, which will be stored in the `public` folder of your project.
- Using an AJAX request, get data from the array of products, write it to the local state of the main page component.
- Display the list of products on the page. The design can be taken from the [PSD](./musica.psd) file from the `LATEST ARRIVALS IN MUSICA` section or any of your own. The design can be any, but it should be.
- Product card and product list must be implemented as separate components.
- When clicking the `Add to cart` button, a modal window should appear with confirmation of adding the product to the cart (use the appropriate component from [homework1](../homework1/readme.md).
- Also, each product card must have a star icon, which will allow you to add the product to your favorites. If the product is selected, the star must be painted in any color.
- When adding a product to the cart or to favorites, save the corresponding change in localStorage.
- In the header of the site, show the cart and selected icons, next to which the number of products that have been added to the cart or selected should be indicated.
- The project can be styled using JSS or SCSS.
- All components must be created as ES6 classes.
- Properties passed to components must be checked using `propTypes`.
- `defaultProps` must be specified for all optional properties.
- Currently, the application should have only one page - the main page with a list of products.
