import React, {useState} from 'react';
import {Route, Routes} from "react-router-dom";
import CartPage from "./pages/CartPage/CartPage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import Page404 from "./pages/Page404/Page404";
import HomePage from "./pages/HomePage/HomePage";

const AppRoutes = () => {
    return (
        <>
            <Routes>
                <Route exact path="/" element={<HomePage />}/>
                <Route exact path="/productList" element={<HomePage />}/>
                <Route exact path="/cart" element={<CartPage />}/>
                <Route exact path="/favorites" element={<FavoritePage />}/>
                <Route path="*" element={<Page404 />}/>
            </Routes>
        </>
    );
};

export default AppRoutes;