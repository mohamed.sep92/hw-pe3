## Assignment

Plug in the Redux library.

For this, it is necessary to supplement the project created in the previous homework [homework3](../homework3/readme.md).

#### Technical requirements:
- Connect the `redux`, `react-redux` and `redux-thunk` libraries.
- After receiving an array of data using an AJAX request, write them to the `redux store`.
- When displaying components on the page - data is taken from `redux store`.
- When opening any modal windows - information about whether a modal window is currently open should be stored in the `redux store`.
- All actions must be performed as functions using the `redux-thunk` functionality.