## Exercise

Create a new React app with two modals.

#### Technical requirements:
- Create an app with `create-react-app`.
- Create 2 buttons on the main page with the text `Open first modal` and `Open second modal`.
- By clicking on each of the buttons, the corresponding modal window should open.
- Create a `Button` component which should have the following properties passed from the parent component:
   - Background color (`backgroundColor` property)
   - Text (`text` property)
   - Function on click (`onClick` property)
- Create a `Modal` component which should have the following properties passed from the parent component:
   - Modal title text (`header` property)
   - Should there be a cross to close the window at the top right (boolean property `closeButton`, values `true/false`)
   - The main text of the modal window, which will be shown in its central part (`text` property)
   - Buttons that are at the bottom of the modal window, passed as code in JSX format (the `actions` property)
- With the modal window open, the rest of the page should be darkened with a dark translucent background.
- The modal window should close when clicking on the shaded area outside of its content.
- Style buttons and modals with SCSS
- Buttons should be different colors
- Modal windows should contain different text.
- Modal window design is given in [PSD](./modal-window.psd) file.
- Make one modal window as in the design example. For the second one, you need to use different text and different buttons (select any).
- All components must be created as ES6 classes.

#### Optional advanced task
- Style components with JSS or Styled Components instead of SCSS