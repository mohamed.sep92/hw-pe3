## Assignment

Hooks and routing.

To complete this task, you need to supplement the project created in the previous homework [homework2](../homework2/readme.md).

#### Technical requirements:
- Add two pages to the program - Cart and Favorites.
- On these pages, you can use the same product card used on the main page to display information.
- The user must be able to remove the product from the cart. For this, there should be an icon with a cross next to the product card. When deleting from the trash, before the deletion itself, there must be a confirmation of the action - a modal window from the homework [homework1](../homework1/readme.md).
- The user must be able to remove the product from the favorites. Deleting from favorites is done by clicking on the star icon.
- The top menu should have links to all three pages of the program.
- Switching between pages should be done without reloading the page using the `react-router-dom` library.
- Convert all classes into functional components using React hooks.