## Assignment

Add the possibility to buy goods in the basket.

For this, it is necessary to supplement the project created in the previous homework [homework4](../homework4/readme.md).

#### Technical requirements:

Expand the functionality of the program implemented in DZ 1-4.
1. On the shopping cart page, add several fields for receiving information about it and the delivery address. In particular, the form should include the following fields:
  - User name
  - Username
  - Age of the user
  - Delivery address
  - Cellphone
2. Add minimal validations (text, number) to the fields. All fields must be filled in.
3. Add the `Checkout` button, which will simulate the process of buying goods in the basket.

General scheme:
  - We create and display a component with a form.
  - When entering values, we perform validations for `touched` fields. If something is wrong, we display an error message under the field.
  - If the information in the fields is correct, after clicking on `Checkout`, we call the action, which in turn clears the data about the cart from `localStorage`. At the same time, information about purchased goods, as well as information that the user filled in the form, should be displayed in the console.

4. To read the data entered in the form, as well as for their validation, it is absolutely necessary to use the libraries `formik` and `yup`.

#### An optional task of increased complexity

Show the user a mask on the mobile phone input field in the form of `(###)###-##-##`. For this, you can use the [react-number-format](https://github.com/s-yadav/react-number-format) library.
