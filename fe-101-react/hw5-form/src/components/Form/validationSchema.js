import * as yup from 'yup';

const schemas = yup.object().shape({
    FirstName: yup
        .string()
        .required('This field is required')
        .min(3, 'Min lenght is 3 symbol'),
    LastName: yup
        .string()
        .required('This field is required')
        .min(3, 'Min lenght is 3 symbol'),
    Age: yup
        .number('It must be number')
        .required('this field is required')
        .min(16, 'Your age under 16 years old, access denied')
        .max(120, 'Please, enter correct value'),
    Adress: yup
        .string()
        .required('This field is required')
        .min(10, 'Min lenght is 10 symbol'),
    Phone: yup
        .number()
        .required('this field is required')
        .min(380000000000, "Number can consist of 13 numbers and started with 380")
        .max(381000000000, "Number can consist of 13 numbers and started with 380")

});

export default schemas