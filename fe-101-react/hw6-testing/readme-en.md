## Assignment

Write unit tests for an online store. Add Context API usage to the application.

For this, it is necessary to supplement the project created in the previous homework [homework5](../homework5/readme.md).

#### Technical requirements:
  - Write several unit tests to test all buttons and modal windows of the application.
  - Write unit tests for reducers
  - Write several snapshot tests to test the simplest components
  - You can use Jest or Testing Library for testing.
  - Add a product display type switch on the product list page - show them in the form of a table or cards. This switch must work using the Context API.